﻿using laboratoire1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace laboratoire1.Controllers
{
    public class ProfController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult profLogin() 
        {
            return View();
        }
        // GET: prof

        [HttpPost]
        public ActionResult profLogin(prof temp)
        {
            HttpCookie cookie = new HttpCookie("profInfo");

            linqDataContext db = new linqDataContext();

            prof pr = (from u in db.profs
                        where u.username == temp.username &&
                          u.password == temp.password
                       select u).SingleOrDefault<prof>();


            temp = pr;

            if (pr != null)
            {
                cookie.Values.Add("nom", temp.nom);
                cookie.Values.Add("prenom", temp.prenom);
                cookie.Values.Add("idProf", temp.IdProf.ToString());
                HttpContext.Response.Cookies.Add(cookie);
                return RedirectToAction("profCours", temp);

            }


            else
            {
                ViewBag.Message = "Username ou password incorrects";
            }
            return View();
        }

        public ActionResult profCours(prof temp)
        {
            HttpCookie cookie = Request.Cookies.Get("profInfo");
            if (cookie != null)
            {

                prof pr  = new prof();
                pr = temp;

                linqDataContext db = new linqDataContext();

                List<Cour> liste = (from c in db.Cours
                                    where c.IdProf == int.Parse(cookie["idProf"])
                                    select c).ToList<Cour>();



                if (liste != null)
                {
                    ViewBag.data = liste;

                }
                else
                {
                    Response.Write("Aucun cours disponible");
                }


                return View(liste);
            }
            else
            {
                return RedirectToAction("profLogin");

            }
        }

       
        public ActionResult detailCours(Cour cours)
        {



                linqDataContext db = new linqDataContext();

                List<Exercice> ExerciceList = (from ex in db.Exercices
                                    where ex.idCours == cours.IdCours
                                    select ex).ToList<Exercice>();

                List<Video> VideoList = (from vid in db.Videos
                                           where vid.idCours == cours.IdCours
                                           select vid).ToList<Video>();


                List<Solutionnaire> SolutionnaireList = (from sol in db.Solutionnaires
                                       where sol.idCours == cours.IdCours
                                       select sol).ToList<Solutionnaire>();

                List<NotesCour> NotesCoursList = (from nc in db.NotesCours
                                                     where nc.idCours == cours.IdCours
                                                     select nc).ToList<NotesCour>();

                List <Labo> LabosList = (from lb in db.Labos
                                                  where lb.idCours == cours.IdCours
                                                  select lb).ToList<Labo>();



            if (ExerciceList!=null)
            {
                ViewBag.exercice = ExerciceList;

            }
            else
            {
                Response.Write("Aucun exercice disponible");
            }

            if(VideoList !=null)
            {
                ViewBag.video = VideoList;
            }
            else
            {
                Response.Write("Aucune video pour ce cours");
            }

            if (NotesCoursList != null)
            {
                ViewBag.notescours = NotesCoursList;
            }
            else
            {
                Response.Write("Aucune note de cours disponible");
            }

            if (LabosList != null)
            {
                ViewBag.labo = LabosList;
            }
            else
            {
                Response.Write("Aucun laboratoire pour ce cours");
            }
            if (SolutionnaireList != null)
            {
                ViewBag.solutionnaire = SolutionnaireList;
            }
            else
            {
                Response.Write("Aucun solutionnaire pour ce cours");
            }

            return View();


        }

        public ActionResult exercice()
        {


            return View();
        }
    }


}