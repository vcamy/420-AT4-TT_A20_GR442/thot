﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using laboratoire1.Models;
using System.Collections;


namespace laboratoire1.Controllers
{
    public class StudentController : Controller
    {

        //GET
 
        public ActionResult Index()
        {
            return View();
        }



        public ActionResult inscriptionStudent()
        {
            return View();
        }

        [HttpPost]
        public ActionResult inscriptionStudent(Student temp)
        {
            //recuperer les données du form
            string nom = temp.nom;
            string prenom = temp.prenom;
            string email = temp.email;
            int nivScol = temp.IdNivScolaire;

            //nombre de connexion = 0 (inscription)
            int nbconnect = 0;


            
            //generer le username
            string username = temp.nom.Substring(0, 3) + temp.prenom.Substring(0, 3);

            //generer un password
            Random rnd = new Random();
            string password = rnd.Next(1, 999) + temp.nom.Substring(0, 2) + temp.prenom.Substring(0, 2) + rnd.Next(1, 999);

            //liste qui va recuperer toutes les variables
            List<Student> liste = new List<Student>();
            //ajout a la liste
            liste.Add(new Student { nom = nom, prenom = prenom, email = email, IdNivScolaire = nivScol, username = username, password = password, nbconnect = nbconnect });

            //pousser le nouvel objet dans la table linq
            linqDataContext db = new linqDataContext();

            db.Students.InsertAllOnSubmit(liste);
            db.SubmitChanges();

            //email
            if (sendMail(temp, username, password))
            {
                ViewBag.mailStatus = "Vos informations de connexion vous ont ete envoyees par courriel.";
                return RedirectToAction("studentLogin");
            }
            else
            {
                ViewBag.mailStatus = "Un probleme est survenu. Votre inscription est incomplete. Merci de completer le formulaire au complet.";
                return View("inscriptionStudent");
            }

        }

        
        public bool sendMail(Student temp, string username, string password)
        {
            try
            {
                string subject = "Inscription complétée : informations pour la connexion";
                string message = "Bonjour, merci de rejoindre la plateforme de e-learning. Vos informations de connexion. Username : " + username + ",  password : " + password;
                string MailSender = System.Configuration.ConfigurationManager.AppSettings["MailSender"].ToString();
                string pw = System.Configuration.ConfigurationManager.AppSettings["MailPw"].ToString();

                SmtpClient smtpClient = new SmtpClient("smtp.office365.com", 587);

                smtpClient.EnableSsl = true;
                smtpClient.Timeout = 100000;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(MailSender, pw);

                MailMessage mailMessage = new MailMessage(MailSender, temp.email, subject, message);

                mailMessage.BodyEncoding = System.Text.UTF8Encoding.UTF8;

                smtpClient.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }


        public ActionResult studentLogin()
        {
            return View();
        }

        public ActionResult LogOut()
        {
            HttpCookie cookie = Request.Cookies.Get("userInfo");
            if(cookie != null)
            {
                cookie.Expires = DateTime.Now.AddDays(-1);

                HttpContext.Response.Cookies.Add(cookie);
                return RedirectToAction("studentLogin");
            }
            else
            {
                return RedirectToAction("listeCours");
            }
            
        }


        [HttpPost]
        public ActionResult studentLogin(Student temp)

        {
            linqDataContext db = new linqDataContext();

            HttpCookie cookie = new HttpCookie("userInfo");

            Student st = (from u in db.Students
                          where u.username == temp.username &&
                          u.password == temp.password
                          select u).SingleOrDefault<Student>();

            

            if (st != null)
            {
                temp.email = st.email;
                temp.Id = st.Id;
                temp.IdNivScolaire = st.IdNivScolaire;
                temp.nbconnect = st.nbconnect;
                temp.nom = st.nom;
                temp.prenom = st.prenom;
                temp.username = st.username;
                temp.password = st.password;

                cookie.Values.Add("nom", temp.nom);
                cookie.Values.Add("prenom", temp.prenom);
                cookie.Values.Add("idNivScolaire", temp.IdNivScolaire.ToString());
                HttpContext.Response.Cookies.Add(cookie);

                


                if (st.nbconnect < 1)
                {
                    st.nbconnect = 1;
                    db.SubmitChanges();

                    return RedirectToAction("changePw", temp);

                }
                else
                {



                    return RedirectToAction("listeCours",temp);

                }
            }

            
            else
            {
                return RedirectToAction("inscriptionStudent");
            }

        }
        public ActionResult changePw()
        {
            return View();
        }

        [HttpPost]
        public ActionResult changePw(Student temp)
        {

            linqDataContext db = new linqDataContext();

            Student st = (from u in db.Students
                          where u.nom == temp.nom &&
                          u.prenom == temp.prenom && u.email == temp.email
                          select u).SingleOrDefault<Student>();

            st.username = temp.username;
            st.password = temp.password;

            db.SubmitChanges();


            if (st != null)
            {
                Response.Write("Username et Mot de passe bien modifies");
            }
            else
            {
                Response.Write("Echec de la modification");
            }


            return RedirectToAction("listeCours", st);
        }

        public ActionResult listeCours(Student temp)
        {
            HttpCookie cookie = Request.Cookies.Get("userInfo");
            if(cookie != null)
            {

                Student st = new Student();
                st = temp;

                linqDataContext db = new linqDataContext();

                List<Cour> liste = (from c in db.Cours
                                    join ns in db.NivScolaires
                                    on c.IdCours
                                    equals ns.idCours
                                    where ns.IdNivScolaire == int.Parse(cookie["idNivScolaire"])
                                    select c).ToList<Cour>();



                

                

                if (liste != null)
                {
                    ViewBag.data = liste;

                }
                else
                {
                    Response.Write("Aucun cours disponible");
                }


                return View(liste);
            }
            else
            {
                return RedirectToAction("studentLogin");

            }


        }

        public ActionResult studentCours(Cour cours)
        {



            linqDataContext db = new linqDataContext();

            List<Exercice> ExerciceList = (from ex in db.Exercices
                                           where ex.idCours == cours.IdCours
                                           select ex).ToList<Exercice>();

            List<Video> VideoList = (from vid in db.Videos
                                     where vid.idCours == cours.IdCours
                                     select vid).ToList<Video>();


            List<Solutionnaire> SolutionnaireList = (from sol in db.Solutionnaires
                                                     where sol.idCours == cours.IdCours
                                                     select sol).ToList<Solutionnaire>();

            List<NotesCour> NotesCoursList = (from nc in db.NotesCours
                                              where nc.idCours == cours.IdCours
                                              select nc).ToList<NotesCour>();

            List<Labo> LabosList = (from lb in db.Labos
                                    where lb.idCours == cours.IdCours
                                    select lb).ToList<Labo>();



            if (ExerciceList != null)
            {
                ViewBag.exercice = ExerciceList;

            }
            else
            {
                Response.Write("Aucun exercice disponible");
            }

            if (VideoList != null)
            {
                ViewBag.video = VideoList;
            }
            else
            {
                Response.Write("Aucune video pour ce cours");
            }

            if (NotesCoursList != null)
            {
                ViewBag.notescours = NotesCoursList;
            }
            else
            {
                Response.Write("Aucune note de cours disponible");
            }

            if (LabosList != null)
            {
                ViewBag.labo = LabosList;
            }
            else
            {
                Response.Write("Aucun laboratoire pour ce cours");
            }
            if (SolutionnaireList != null)
            {
                ViewBag.solutionnaire = SolutionnaireList;
            }
            else
            {
                Response.Write("Aucun solutionnaire pour ce cours");
            }

            return View();


        }

    }
}
